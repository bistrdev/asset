import 'jquery';
import 'bootstrap-sass';
import 'simpleweather';
import 'timeago';
import 'timeago/locales/jquery.timeago.ro';
import 'malihu-custom-scrollbar-plugin';
import logo from '../images/bistriteanul_logo.png';

let hostname = 'http://cdn.robery.eu/';

function initDate() {
    let dayName = [
        "Luni",
        "Marți",
        "Miercuri",
        "Joi",
        "Vineri",
        "Sâmbătă",
        "Duminică"
    ];

    let monthName = [
        "Ianurie",
        "Februarie",
        "Martie",
        "Aprilie",
        "Mai",
        "Iunie",
        "Iulie",
        "August",
        "Septembrie",
        "Octombrie",
        "Noiembrie",
        "Decembrie"
    ];

    let dateNow = new Date();

    $("#date").text(dayName[dateNow.getDay()] + ', ' + dateNow.getDate() + ' ' + monthName[dateNow.getMonth()] + ' ' + dateNow.getFullYear());
}

function generateLogo() {
    let target = $('a[id="logo"]'),
        generated = $('<img/>').addClass('logo').attr('width', 379).attr('height', 64).attr('alt', 'bistriteanul.ro').attr('title', 'bistriteanul.ro').attr('src', hostname + logo);

    target.append(generated);
}

function initWeather() {
    let latitude = '47.1392617',
        longitude = '24.4890979';

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
        });
    }

    loadWeather(latitude + ',' + longitude);
}

function loadWeather(location, woeid) {
    $.simpleWeather({
        location: location,
        woeid: woeid,
        unit: 'c',
        success: (weather) => {
            let html;
            html = '<i id="weather-icon" class="icon-' + weather.code + '"></i> ' + weather.city + ' (' + weather.temp + ' &deg;' + weather.units.temp + ')';

            $("#weather").html(html);
        },
        error: (error) => {
            $("#weather").html(error);
        }
    });
}

jQuery(() => {
    initDate();
    initWeather();
    generateLogo();

    jQuery('time').timeago();
});
